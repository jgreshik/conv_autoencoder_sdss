# conv_autoencoder_SDSS

Simple convolutional autoencoder made to train with 28x28 pixel galaxy, star and qso data  

Visualizations may have taken place atemporally in jupyter notebook. Make sure model is properly trained
before attempting to plot images.
